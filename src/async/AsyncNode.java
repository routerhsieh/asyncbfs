package async;

import java.util.*;
import java.util.concurrent.locks.*;

public class AsyncNode implements Communicator.MessageReceiver {
	private int id;
	private int logicalParent = 0;
	private Set<Integer> physicalNeighbors = new HashSet<Integer>();
	private Set<Integer> logicalChilds= new HashSet<Integer>();
	private Map<Integer, Complete> completion = new HashMap<Integer, Complete>();
	private List<Integer> currentPath = null;
	private final Lock lock = new ReentrantLock(true);
	private Random random = new Random();
	private Communicator communicator;
	private boolean isDone = false;
	
	public static class Complete {
		boolean completed = false;
		
		void setCompleted(boolean completed) {
			this.completed = completed;
		}
		
		boolean isCompleted() {
			return this.completed;
		}
	}
	
	public AsyncNode(int id, Communicator communicator) {
		this.id = id;
		this.communicator = communicator;
	}
	
	private Message createMessage(MessageType messageType, int from, int to, List<Integer> path) {
		Message message = new Message(messageType, from, to, path, random.nextInt(15) + 1);
		return message;
	}
	
	private void printChilds() {
		if (logicalChilds.size() == 0) {
			// System.out.printf("Node %d is leaf\n", id);
		} else {
			if (!isDone) {
				System.out.printf("Node %d's parent is %d\n", id, logicalParent);
				System.out.printf("Node %d's childs:\n", id);
				for (Integer child : logicalChilds) {
					System.out.printf("%d ", child);
				}
				System.out.println("");
				isDone = true;
			}
		}
	}
	
	private void joinAction(Message message) {
		//1.	Check incoming path's distance is shorter current or not
		//2-1.	If its distance is shorter, then switch parent to new one, 
		//		send JOIN_ACK to new parent, send DISJOIN to old parent, 
		//		reset the completion, then re-broadcast new JOIN message to (possible) childs
		//2-2.	Otherwise, just reply NON_JOIN_ACK to this message's sender
		int currentPathLength;
		if (currentPath != null) {
			currentPathLength = currentPath.size();
		}
		else {
			currentPathLength = Integer.MAX_VALUE;
		}
		
		int incomingPathLength = message.getPath().size();
		int sender = message.getFrom();
		
		if (incomingPathLength + 1 < currentPathLength) {
			if (logicalParent != 0) {
				Message disjoin = createMessage(MessageType.DISJOIN, id, logicalParent, null);
				communicator.send(disjoin);
			}
			
			int oldParent = logicalParent;
			logicalParent = message.getFrom();
			currentPath = new LinkedList<Integer>(message.getPath());
			currentPath.add(id);
			Message joinAck = createMessage(MessageType.JOIN_ACK, id, logicalParent, null);
			communicator.send(joinAck);
			
			Set<Integer> neighbors;
			if (oldParent != logicalParent) {
				logicalChilds = new HashSet<Integer>();
				completion = new HashMap<Integer, Complete>();
				neighbors = new HashSet<Integer>(physicalNeighbors);
				neighbors.remove(logicalParent);
			} else {
				neighbors = completion.keySet();
			}
			
			for (Integer neighbor : neighbors) {
				Message join = createMessage(MessageType.JOIN, id, neighbor, currentPath);
				communicator.send(join);
			}
		}
		else {
			//We only need to assign path in JOIN message
			Message nonjoin = createMessage(MessageType.NON_JOIN_ACK, this.id, sender, null);
			communicator.send(nonjoin);
		}
	}
	
	private void joinAckAction(Message message) {
		logicalChilds.add(message.getFrom());
		completion.put(message.getFrom(), new Complete());
	}
	
	private void disjoinAction(Message message) {
		logicalChilds.remove(message.getFrom());
		completion.remove(message.getFrom());
		
		if (checkCompletion()) {
			Message done = createMessage(MessageType.DONE, id, logicalParent, null);
			communicator.send(done);
			printChilds();
		}
	}
	
	private void nonJoinAckAction(Message message) {
		if (checkCompletion()) {
			Message done = createMessage(MessageType.DONE, id, logicalParent, null);
			communicator.send(done);
			printChilds();
		}
	}
	
	private void doneAction(Message message) {
		completion.get(message.getFrom()).setCompleted(true);
		if (checkCompletion()) {
			if (logicalParent == 0) {
				//This is root and it is done, so whole BFS tree is constructed
				Message close = createMessage(MessageType.CLOSE, id, id, null);
				communicator.send(close);
				printChilds();
			} else {
				Message done = createMessage(MessageType.DONE, id, logicalParent, null);
				communicator.send(done);
				printChilds();
			}
		}
	}
	
	private boolean checkCompletion() {
		boolean done = true;
		if (logicalParent != 0 && logicalChilds.size() == 0) {
			return done;
		} else {
			for (Complete complete : completion.values()) {
				done &= complete.isCompleted();
			}
			return done;
		}
	}
	
	private void receiveActions(Message message) {
		switch (message.getMessageType()) {
			case JOIN:
				joinAction(message);
				break;
			case JOIN_ACK:
				joinAckAction(message);
				break;
			case NON_JOIN_ACK:
				nonJoinAckAction(message);
				break;
			case DISJOIN:
				disjoinAction(message);
				break;
			case DONE:
				doneAction(message);
				break;
			default:
				break;
		}
	}
	
	public void addPhysicalNeighbor(int neighbor) {
		this.physicalNeighbors.add(neighbor);
	}
	
	public void find() {
		lock.lock();
		//Only invoke by master thread at root node
		currentPath = new LinkedList<Integer>();
		currentPath.add(id);
		logicalChilds = new HashSet<Integer>(physicalNeighbors);
		for (Integer neighbor : logicalChilds) {
			Message join = createMessage(MessageType.JOIN, id, neighbor, currentPath);
			communicator.send(join);
		}
		lock.unlock();
	}

	@Override
	public void receive(Message message) {
		lock.lock();
		try {
			Thread.sleep(message.getDelay());
			receiveActions(message);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			lock.unlock();
		}
	}
}
