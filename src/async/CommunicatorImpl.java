package async;

import java.util.concurrent.*;
import java.util.*;

public class CommunicatorImpl implements Communicator {
	private volatile boolean running = true;
	private BlockingQueue<Message> messageQueue = new LinkedBlockingQueue<Message>();
	private Map<Integer, MessageReceiver> receivers = new HashMap<Integer, MessageReceiver>();
	private ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	
	
	public CommunicatorImpl() {}
	
	public void doReceive(final Message message) {
		threadPool.execute(new Runnable(){
			@Override
			public void run() {
				/*
				System.out.printf("Debug: from = %d, to = %d, type = %s, time = %d\n", 
						message.getFrom(), message.getTo(), message.getMessageType(), System.currentTimeMillis());
				*/
				MessageReceiver receiver = receivers.get(message.getTo());
				receiver.receive(message);
			}
		});
	}
	
	public void awaitClientSendMessage() {
		while (running) {
			try {
				Message received = messageQueue.take();
				//Only root will send CLOSE message when it is done
				if (received.getMessageType() == MessageType.CLOSE) {
					/*
					System.out.printf("Debug: from = %d, to = %d, type = %s\n", 
							received.getFrom(), received.getTo(), received.getMessageType());
					*/
					running = false;
					return;
				}
				doReceive(received);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void send(final Message message) {
		threadPool.execute(new Runnable() {

			@Override
			public void run() {
				try {
					messageQueue.put(message);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}});
	}

	@Override
	public void setReceiver(int nodeId, MessageReceiver receiver) {
		this.receivers.put(nodeId, receiver);
	}

	@Override
	public void run() {
		awaitClientSendMessage();
	}
}
