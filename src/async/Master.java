package async;

import java.io.*;
import java.util.Scanner;

public class Master {

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Error: Need input file as parameter!!\n");
			return;
		}
		
		String filePath = args[0];
		File inputFile = new File(filePath);
		Scanner scanner = null;
		try {
			scanner = new Scanner(inputFile);
			String[] firstLine = scanner.nextLine().split(",");
			int numberOfNodes = Integer.parseInt(firstLine[0]);
			
			Communicator communicator = new CommunicatorImpl();
			int nodeCounter = 1;
			AsyncNode rootNode = null;
			while (nodeCounter <= numberOfNodes) {
				AsyncNode node = new AsyncNode(nodeCounter, communicator);
				if (nodeCounter == 1) {
					rootNode = node;
				}
				String[] neighborStrings = scanner.nextLine().split("\\s+");
				for (int idx = 0; idx < neighborStrings.length; idx += 1) {
					int connect = Integer.parseInt(neighborStrings[idx]);
					if (connect == 1) {
						node.addPhysicalNeighbor(idx + 1);
					}
				}
				communicator.setReceiver(nodeCounter, node);
				nodeCounter += 1;
			}
			Thread runner = new Thread(communicator);
			runner.start();
			rootNode.find();
			runner.join();
			System.out.printf("AsyncBFS is completed\n");
			return;
		}
		catch (FileNotFoundException | InterruptedException ex) {
			ex.printStackTrace();
		}
		finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
}
