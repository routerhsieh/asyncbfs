package async;

public enum MessageType {
	JOIN, 
	//Source node send this to other neighbor to see if they want to be its child or not
	JOIN_ACK, 
	//If a node receive JOIN from neighbor, either reply JOIN_ACK and set sender as parent,
	//or reply NON_JOIN_ACK to tell the sender it had a parent with shorter distance to root
	NON_JOIN_ACK,
	DISJOIN,
	//When a node discover a shorter path, it should send DISJOIN to old parent, 
	//and send JOIN_ACK to new parent, if the new parent and old parent are different nodes.
	DONE,
	CLOSE;
}
