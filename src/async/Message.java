package async;

import java.util.*;

public class Message {
	private MessageType messageType;
	private int from;
	private int to;
	private long delay;
	private List<Integer> path = new LinkedList<Integer>();
	
	public Message(MessageType messageType, int from, int to, List<Integer> path, long delay) {
		this.messageType = messageType;
		this.from = from;
		this.to= to;
		this.setPath(path);
		this.setDelay(delay);
	}
	
	public MessageType getMessageType() {
		return messageType;
	}
	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}

	public List<Integer> getPath() {
		return path;
	}

	public void setPath(List<Integer> path) {
		this.path = path;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}
}
