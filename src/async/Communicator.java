package async;

public interface Communicator extends Runnable {
	interface MessageReceiver{
		public void receive(Message message);
	}
	
	public void send (Message message);
	public void setReceiver (int nodeId, MessageReceiver receiver);
}
